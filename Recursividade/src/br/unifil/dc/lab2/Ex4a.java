public class Ex4a{
    public static String a(String norm){
        String answer = "";
        String inver = new StringBuffer(norm).reverse().toString();
        if(norm.equals(inver)){
            answer = "É um palíndromo";
        }else{
            answer= "Não é um palíndromo";
        }
        return answer;
    }
}