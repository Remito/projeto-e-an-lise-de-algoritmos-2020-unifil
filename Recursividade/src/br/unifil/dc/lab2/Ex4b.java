public class Ex4b
{
    public static String b(String norm){
        String answer = "";
        norm = norm.replace(" " , "");
        String inver = new StringBuffer(norm).reverse().toString();
        
        if(norm.equals(inver)){
            answer = "É um palíndromo";
        }else{
            answer= "Não é um palíndromo";
        }
        return answer;
    }
}
