public class Ex1
{
    public static int fibonacci(int n) {
        int f0 = 0;
        int f1 = 1;
        int fNovo = 0;
        for (int i = n; i > 0; i--) {
            fNovo = f1 + f0;
            f0 = f1;
            f1 = fNovo;
        }
        return f0;
    }
}