import java.util.ArrayList;
public class Ex5a{
    public static String a(String str[]){
        String answer = "";
        for(int i = 0; i < str.length; i++){
            String norm = str[i];
            norm = norm.replace(" " , "");
            String inver = new StringBuffer(norm).reverse().toString();
            if(norm.equals(inver)){
                answer = "É um palíndromo";
                System.out.println(str[i] + " É um palíndromo");
            }else{
                answer= "Não é um palíndromo";
                System.out.println(str[i] + " Não é um palíndromo");
            }
        }
        return answer;
    }
}
