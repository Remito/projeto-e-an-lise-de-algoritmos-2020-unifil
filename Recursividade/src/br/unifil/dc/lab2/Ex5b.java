public class Ex5b{
    public static String a(){
        String answer = "";
        for(int i = 0; i < 10000; i++){
            String num = i + "";
            String inver = new StringBuffer(num).reverse().toString();
            if(i % 2 == 0){
                if(num.equals(inver)){
                    answer = answer + i + " "; 
                    System.out.println(i);
                }
            }
        }
        return answer;
    }
}
